import {getHeroeById} from './bases/08-imp-exp';

/*

//promesas
const promesa = new Promise( (resolve, reject)=>{   

    setTimeout( ()=>{
        //tarea 
        //importar el 
        const heroe = getHeroeById(2);
        //console.log(heroe);
        //console.log('2 segundos despues');
        resolve( heroe );
        //reject( 'No se pudo encontrar el heroe' );
    },2000)
});

promesa.then( (heroe)=>{
    console.log('heroe', heroe);
} )
.catch(err => console.warn( err ));

*/

const getHeroeByIdAsync = ( id ) => {
    //promesas
    return new Promise( (resolve, reject)=>{   

        setTimeout( ()=>{
            //tarea 
            //importar el 
            const heroe = getHeroeById(id);

            if(heroe){
                resolve( heroe );
            }else{
                reject( 'No se pudo encontrar el heroe' );
            }
            //console.log(heroe);
            //console.log('2 segundos despues');
        },2000)
    });
    
}

getHeroeByIdAsync(1)
    .then(console.log)
    .catch( console.warn);