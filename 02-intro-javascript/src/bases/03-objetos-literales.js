const persona = {
    nombre: 'Shellsea',
    apellido: 'Gonzales',
    edad: 23,
    direccion: {
        ciudad: 'Las Flores',
        zip: 1827891,
        lat: 31.23,
        lng: 33.9238
    }
};

//console.table(persona);
console.log(persona);

//hago un clone del objeto de cada una de las propiedades
const persona2 = {...persona};
persona2.nombre = 'Diana';



console.log(persona2);
