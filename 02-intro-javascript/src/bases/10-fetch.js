const apikey = 'qwrMztQjSu7RKlZRssw5f2qPKoyl7ExO';

const peticion = fetch(`https://api.giphy.com/v1/gifs/random?api_key=${apikey}`);

peticion
    .then( resp => resp.json() )
    .then( ({data}) => {
        const {url} = data.images.original;
        //console.log(url);

        //creo el documento de imagenes
        const img = document.createElement('img')
        img.src = url;

        // lo creo en el html
        document.body.append(img);
    })
    .catch(console.warn);