//Desestructuracion
// Asignacion Desestructurante

const persona = {
    nombre: 'Enrique',
    edad: 25,
    clave: 'Diana',
    
};

//const { nombre, edad, clave } = persona;
//console.log(nombre);
//console.log(edad);
//console.log(clave);

const useContext = ( {clave,nombre, edad, rango = 'Capitan'} ) =>{
    //console.log( nombre, edad, rango);    
    return {
        nombreClave: clave,
        anios:edad,
        latlng: {
            lat:34.123,
            lng:  -12.312
        }
    }
}

const {nombreClave, anios,latlng:{lat,lng}} = useContext(persona);

console.log(nombreClave,anios);
console.log(lat,lng);
